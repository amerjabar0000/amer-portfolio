# Amer Portfolio

### My personal portfolio built in: Gatsby (React.js), Sass and HTML5

#### Hosted on: Netlify
#### Link: https://amer.netlify.app

I have increased the performance, speed and interactiveness of the website by applying most of the best practises I am knowledgeable of in the field of SEO, networking, schema types and compression. The website serves optimized images by the help of the most popular plugins. The fonts are compressed then statically hosted on the same server that hosts the website.

The design of the website is inspired by many creative art-works and I have put the most effort too make it as fancy as possible. It is responsive on all devices and different screens.


